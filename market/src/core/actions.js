export const loadUserCards = (userCards) => ({
  type: "LOAD_USER_CARDS",
  payload: userCards,
});

export const loadRefCards = (refCards) => ({
  type: "LOAD_REF_CARDS",
  payload: refCards,
});

export const loadUser = (user) => ({
  type: "LOAD_USER",
  payload: user,
});
