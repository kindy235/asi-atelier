const initialState = {
  cards: [],
};

export const cardsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOAD_USER_CARDS":
      return {
        ...state,
        userCards: action.payload,
      };
    case "LOAD_REF_CARDS":
      return {
        ...state,
        refCards: action.payload,
      };
    default:
      return state;
  }
};
