const initialState = {
  user: "",
};

export const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOAD_USER":
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
};
