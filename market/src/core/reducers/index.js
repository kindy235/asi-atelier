import { combineReducers } from "redux";
import { cardsReducer } from "./card.reducer";
import { usersReducer } from "./user.reducer";

const globalReducer = combineReducers({
  cardsReducer: cardsReducer,
  usersReducer: usersReducer,
});
export default globalReducer;
