import React from 'react';

const Notification = ({ message }) => {
  return (
    <div style={{
      background: '#f1f1f1',
      borderLeft: '5px solid #2196F3',
      marginTop: '5px',
      padding: '10px',
      boxShadow: '0 2px 4px rgba(0,0,0,0.2)',
    }}>
      {message}
    </div>
  );
};

export default Notification;
