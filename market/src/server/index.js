const express = require('express');
const { Pool } = require('pg');
const cors = require('cors');

const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());

const pool = new Pool({
  user: 'your_pgadmin_user',
  host: 'localhost',
  database: 'your_database_name',
  password: 'your_pgadmin_password',
  port: 5432,
});

app.post('/login', async (req, res) => {
  const { username, password } = req.body;

  try {
    const result = await pool.query('SELECT * FROM users WHERE username = $1 AND password = $2', [username, password]);
    
    if (result.rows.length > 0) {
      res.json({ success: true });
    } else {
      res.json({ success: false, error: "Nom d'utilisateur ou mot de passe incorrect!" });
    }
  } catch (error) {
    console.error('Erreur lors de la connexion:', error.stack);
    res.status(500).json({ success: false, error: 'Erreur serveur!' });
  }
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
