import React, { useState } from 'react';
import Register from './Register';
import { Input, Button, Form, Container } from 'semantic-ui-react';
import UserNotification from "../Notification/User/UserNotification";

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [showRegister, setShowRegister] = useState(false);

  UserNotification();

  const handleLogin = async () => {
    try {
        const response = await fetch('/auth-service/auth/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ username, password }),
          credentials: 'include',
        });

        if (!response.ok) {
          throw new Error("Erreur d'authentification", response);
        }
        
        await response.text();
        window.location.reload();

    } catch (error) {
      console.error('Erreur de création de compte :', error.message);
      setError(error.message);
    }
  };

  const toggleShowRegister = () => {
    setShowRegister(!showRegister);
  };

  return (
    <Container text style={{ marginTop: '50px', textAlign: 'center', with: "200px" }}>
      <div>
      <Form>
        <Form.Field>
          <label>Username</label>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Enter your username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <Input
            type="password"
            icon="lock"
            iconPosition="left"
            placeholder="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Field>
        <Button type="button" onClick={handleLogin}>
          Login
        </Button>
        <Button type="button" onClick={toggleShowRegister}>
          Créer un compte
        </Button>
      </Form>
      </div>
      
      {showRegister && <Register />}
        {error && <div style={{ color: 'red' }}>{error}</div>}
    </Container>
  );

};

export default Login;
