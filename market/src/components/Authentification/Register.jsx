import React, { useState } from 'react';
import { Form, Input, Button } from 'semantic-ui-react';

const Register = () => {
  const [formData, setFormData] = useState({
    login: '',
    pwd: '',
    lastName: '',
    account: 0,
    surName: '',
    email: '',
  });
  const [error, setError] = useState('');

  const handleCreateAccount = () => {
    // Vérification que tous les champs sont remplis
    for (const key in formData) {
      if (formData[key] === '') {
        setError(`Veuillez remplir tous les champs`);
        return;
      }
    }

    // Vérification que le prénom et le nom ne contiennent pas de chiffres ou de caractères spéciaux
  const nameRegex = /^[A-Za-zÀ-ÖØ-öø-ÿ]+$/;
  if (!nameRegex.test(formData.surName) || !nameRegex.test(formData.lastName)) {
    setError('Le prénom et le nom ne peuvent contenir que des lettres');
    return;
  }

  // Vérification du mot de passe
  if (formData.pwd.length < 5) {
    setError('Le mot de passe doit contenir au moins 5 caractères');
    return;
  }

    // Vérification logique pour l'adresse email
    if (!formData.email.includes('@')) {
      setError('Adresse email invalide');
      return;
    }

    console.log(formData);

    fetch('http://localhost:8000/user-service/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Erreur lors de la création du compte", response);
        } else {
          return response.text();
        }
      }).then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.error('Erreur de création de compte :', error.message);
        setError('Erreur : ' + error.message);
      });
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
        <div>
      <Form>
        <Form.Field>
          <label>Username :</label>
          <Input
            type="text"
            name="login"
            value={formData.login}
            onChange={handleChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Mot de passe :</label>
          <Input
            type="password"
            name="pwd"
            value={formData.pwd}
            onChange={handleChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Prénom :</label>
          <Input
            type="text"
            name="surName"
            value={formData.surName}
            onChange={handleChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Nom :</label>
          <Input
            type="text"
            name="lastName"
            value={formData.lastName}
            onChange={handleChange}
          />
        </Form.Field>

        <Form.Field>
          <label>Mail :</label>
          <Input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
          />
        </Form.Field>

        <Button type="button" onClick={handleCreateAccount}>
          Enregister
        </Button>

        {error && <div style={{ color: 'red' }}>{error}</div>}
      </Form>
    </div>
  );
};

export default Register;