import React, { useEffect, useState } from 'react';
import Header from '../Home/Header';
import CardList from '../Card/CardList';
import { useDispatch, useSelector } from "react-redux";
import { selectRefCards, selectUser } from "../../core/selectors";
import { loadRefCards, loadUserCards } from "../../core/actions";
import { useParams } from 'react-router-dom'; // Import useParams from react-router-dom
import CardListGame from '../Card/CardListGame';
import CardListPlay from '../Card/CardListPLay';
import CardListOpponent from '../Card/CardListOpponent';
import { Input, Button, Form, Container, Select } from 'semantic-ui-react';

const Room = () => {
  const { roomcode } = useParams(); // Capture the model ID from the URL
  const dispatch = useDispatch();
  const [isJoined, setIsjoined] = useState(false); // State to indicate if max limit is reached
  const [cardsPlayer1, setCardsPlayer1] = useState([]);
  const [cardsPlayer2, setCardsPlayer2] = useState([]);
  const [player1Id, setPLayer1Id] = useState(null); // State to indicate if max limit is reached
  const [activePlayer, setActivePlayer] = useState(null); // State to indicate if max limit is reached
  const [winnerPlayer, setWinnerPlayer] = useState(null); // State to indicate if max limit is reached
  const [attackCardId, setAttackCardId] = useState(null); // State to indicate if max limit is reached
  const [beAttackCardId, setBeAttackCardId] = useState(null); // State to indicate if max limit is reached
  const [errorMessage, setErrorMessage] = useState(""); // State to indicate if max limit is reached

  const refreshRoom = async () => {
    try {
      const response = await fetch(`http://localhost:3200/getGame?gameCode=${roomcode}`);

      if (response.ok) {
        const game = await response.json();
        console.log("game : ", game);
        const { player2 } = game;
        const { player1 } = game;
        const { activePlayerId } = game;
        const { id } = player2;
        const { winnerPlayerId } = game
        setWinnerPlayer(winnerPlayerId)
        console.log("1 winner id : ", winnerPlayerId);
        if (id != -1) {
          //console.log("joueur 2 a join");
          setIsjoined(true);
          setCardsPlayer2(player2.cards)
          setCardsPlayer1(player1.cards)
          setActivePlayer(activePlayerId)
          setPLayer1Id(player1.id)
        }
      }
    } catch (error) {
      setErrorMessage(error.message)
      console.error('Erreur :', error.message);
    }
  };

  const playerAttack = async () => {
    try {
      const response = await fetch('http://localhost:3200/playerAttack/', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          gameCode: roomcode,
          CarteSource: attackCardId,
          CarteDest: beAttackCardId,
        })
      });

      if (response.ok) {
        console.log("Attack Reussi");
        setAttackCardId(null)
        setBeAttackCardId(null)
        refreshRoom()
      }
    } catch (error) {
      console.error('Erreur :', error.message);
      setErrorMessage(error.message)
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      if (isJoined != 1) {
        //console.log("IS JOINED : ", isJoined)
        refreshRoom();
      }
    }, 2000);

    return () => clearInterval(interval);
  }, [roomcode]);

  const user = useSelector(selectUser);

  return (
    <div>
      <Header user={user} title={"ROOM"} desc={"Room for game"} />
      <div class="ui message"><p>Room code : {roomcode}</p></div>
      {!isJoined ? (
        <div>
          <p>Waiting player</p>
        </div>
      ) : (
        <div>
          <div class="ui message"><p>New player JOIN Game</p></div>
          {errorMessage && (
            <div className="ui red message">
              <p>Erreur : {errorMessage}</p>
            </div>
          )}
          {winnerPlayer !== -1 ? (
            winnerPlayer === user.id ? (
              <div className="ui green center aligned header huge"><p>You Win !</p></div>
            ) : (
              <div className="ui red center aligned header huge"><p>You Loose...</p></div>
            )
          ) : (
            activePlayer == user.id ? (
              <div>
                {player1Id == user.id ? (
                  <div class="ui message">
                    <p>Select your card id which will attack : <select
                      value={attackCardId}
                      onChange={(e) => setAttackCardId(e.target.value)}
                    >
                      <option value="" disabled selected hidden>select a card ID</option>
                      {cardsPlayer1.map(card => (
                        card.hp !== 0 ? (
                          <option key={card.id} value={card.id}>{card.id}</option>
                        ) : null
                      ))}
                    </select></p>

                    <p>select enemy's card id which will be attacked : <select
                      value={beAttackCardId}
                      onChange={(e) => setBeAttackCardId(e.target.value)}
                    >
                      <option value="" disabled selected hidden>select a card ID</option>
                      {cardsPlayer2.map(card => (
                        card.hp !== 0 ? (
                          <option key={card.id} value={card.id}>{card.id}</option>
                        ) : null
                      ))}
                    </select></p>
                    {attackCardId !== null && beAttackCardId !== null && (
                      <Button type="button" onClick={playerAttack}>Attack !</Button>
                    )}
                    <br />
                    <div class="ui message">
                      <p>Your Cards</p>
                      <CardListPlay cards={cardsPlayer1} actionName={"attack"} />
                      <br />
                      <p>Enemy's Cards</p>
                      <CardListOpponent cards={cardsPlayer2} actionName={"attack"} />
                    </div>
                  </div>
                ) : (
                  <div class="ui message">
                    <p>select your card id which will attack : <select
                      value={attackCardId}
                      onChange={(e) => setAttackCardId(e.target.value)}
                    >
                      <option value="" disabled selected hidden>select a card ID</option>
                      {cardsPlayer2.map(card => (
                        card.hp !== 0 ? (
                          <option key={card.id} value={card.id}>{card.id}</option>
                        ) : null
                      ))}
                    </select> </p>

                    <p>select enemy's card id which will be attacked : <select
                      value={beAttackCardId}
                      onChange={(e) => setBeAttackCardId(e.target.value)}
                    >
                      <option value="" disabled selected hidden>select a card ID</option>
                      {cardsPlayer1.map(card => (
                        card.hp !== 0 ? (
                          <option key={card.id} value={card.id}>{card.id}</option>
                        ) : null
                      ))}
                    </select></p>

                    {attackCardId !== null && beAttackCardId !== null && (
                      <Button type="button" onClick={playerAttack}>Attack !</Button>
                    )}
                    <br />
                    <div class="ui message">
                      <p>Your Cards</p>
                      <CardListPlay cards={cardsPlayer2} actionName={"attack"} />
                      <br />
                      <p>Enemy's Cards</p>
                      <CardListOpponent cards={cardsPlayer1} actionName={"attack"} />
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <div class="ui message">
                <p>Waiting your turn...</p>

                {player1Id === user.id ? (
                  <div>
                    <div>
                      <p>Your Cards</p>
                      <CardListPlay cards={cardsPlayer1} actionName={"attack"} />
                      <br />
                      <p>Enemy's Cards</p>
                      <CardListOpponent cards={cardsPlayer2} actionName={"attack"} />
                    </div>
                  </div>
                ) : (
                  <div>
                    <div>
                      <p>Your Cards</p>
                      <CardListPlay cards={cardsPlayer2} actionName={"attack"} />
                      <br />
                      <p>Enemy's Cards</p>
                      <CardListOpponent cards={cardsPlayer1} actionName={"attack"} />
                    </div>
                  </div>
                )}
              </div>
            )
          )}
        </div>
      )}
    </div>
  );
}
export default Room;
