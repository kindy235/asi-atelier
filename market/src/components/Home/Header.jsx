import React from 'react';
import Logout from './Logout'; // Assurez-vous que le chemin d'import est correct

const Header = ({user, title, desc}) => {
  return (
    <div class="ui clearing segment">
    <h3 class="ui right floated header">
        <Logout />
        <i class="user circle outline icon"></i>
        <div class="content">   
            <span id="userNameId">{user.login}</span>
            <div class="sub header"><span>{user.account}</span>$</div>
        </div>
    </h3>

    <h3 class="ui left floated header">
        <i class="game icon"></i>
        <div class="content">
            {title}
            <div class="sub header">{desc}</div>
        </div>
    </h3>
    
</div>

    
  );
};

export default Header;
