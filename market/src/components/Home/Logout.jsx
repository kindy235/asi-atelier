import { Button } from "semantic-ui-react";
import React, { useEffect } from "react";
import Cookies from "js-cookie";
import { useDispatch } from "react-redux";
import { loadUser } from "../../core/actions";
const Logout = () => {
  const dispatch = useDispatch();
  const checkLogoutStatus = async () => {
    try {
      const response = await fetch("/auth-service/auth/logout", {
        method: "POST",
        credentials: "include",
      });
      if (response.ok) {
        dispatch(loadUser(''));
      }
    } catch (error) {
      console.error(
        "Erreur lors de la vérification de la déconnexion :",
        error.message
      );
    }
  };

  const HandleLogout = async () => {
    // Supprimer le cookie de session
    await checkLogoutStatus();
    Cookies.remove("userId");
    // Rafraîchir la page
    window.location.reload();
  };

  return <Button onClick={HandleLogout}>Déconnexion</Button>;
};

export default Logout;
