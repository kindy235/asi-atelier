// ActionButton.js
import React from 'react';
import { Button, Icon } from 'semantic-ui-react';

const ActionButton = ({ buttonText, icon}) => {
  return (
    <Button size="massive">{buttonText} <Icon name={icon}/></Button>
  );
};

export default ActionButton;
