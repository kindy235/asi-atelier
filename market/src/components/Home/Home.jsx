import React, { useEffect } from 'react';
import Header from './Header'; 
import { Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import ActionButton from './ActionButton';
import { useSelector } from "react-redux";
import { selectUser } from "../../core/selectors";

const Home = () => {
    // const dispatch = useDispatch();
    // let user_id = Cookies.get('userId');

    // useEffect(() => {
    //   async function fetchData() {
    //     // You can await here
    //     const resp = await fetch('http://localhost:8082/user/' + user_id);
  
    //     const result = await resp.json();
    //     dispatch(loadUser(result));
    //   }
    //   fetchData();
    // }, [dispatch]);
    
    // dispatch(loadUser(result));

    const storedUser = useSelector(selectUser);
    
    return (
      <div>
        <Header user={storedUser} title={"Card Game"} desc={"Welcome to card game"} />
        <Container text style={{ marginTop: '50px', textAlign: 'center', with: "100%" }}>
          <Link to="/sell">
            <ActionButton buttonText="Sell" icon="bitcoin icon" />
          </Link>
          <Link to="/buy">
            <ActionButton buttonText="Buy" icon="shop icon" />
          </Link>
          <Link to="/play">
            <ActionButton buttonText="Play" icon="play icon" />
          </Link>
        </Container>
      </div>
    );
};



export default Home;
