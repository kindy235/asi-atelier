import { useEffect } from 'react';
import { io } from 'socket.io-client';


const StoreNotification = () => {


  useEffect(() => {

    // store service 
    const socket = io('http://localhost:3001');

    socket.on('store/notification', (notification) => {
      try {
          // let data = JSON.parse(notification)
          const { action, result, payload } = notification;
          if ((action==="buy" || action==="sell") && result==="progress") {
            alert("Opération en cours...");
          }
          else if ((action==="buy" || action==="sell") && result==="success" && payload) {
            alert("Operation réalisée avec succès !");
            window.location.reload();
          }else{
            alert("echec de l'opération !");
          }
      } catch (error) {
        alert(error.message)
      }
  
      console.log('Message reçu:', notification);
    });
    
    // Nettoyer en cas de démontage du composant
    return () => socket.off('store/notification');
  }, []);

  return;
};

export default StoreNotification;
