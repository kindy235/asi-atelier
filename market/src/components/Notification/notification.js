const express = require('express');
const stompit = require('stompit');
const http = require('http');
const { Server } = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = new Server(server);

app.use(express.json());

const ACTIVE_MQ_URL = 'tcp://localhost:61613';

const services = {
  userService: '/queue/UserServiceQueue',
  storeService: '/queue/StoreServiceQueue',
  cardModeService: '/queue/CardModeServiceQueue',
};

// Connecter à ActiveMQ
const connectActiveMQ = (queueName, callback) => {
  const connectOptions = {
    'host': 'localhost',
    'port': 61613,
    'connectHeaders':{
      'host': '/',
      'login': 'myuser', // Les identifiants par défaut d'ActiveMQ
      'passcode': 'mypwd',
      'heart-beat': '5000,5000'
    }
  };

  console.log(`Trying to connect to ${queueName}`);
  stompit.connect(connectOptions, (error, client) => {
    if (error) {
      console.log('Connection error ' + error.message);
      return;
    }
    
    console.log(`Connected to ${queueName}`);
    callback(client, queueName);
  });
};

// Fonction pour envoyer des notifications
const sendNotification = (queueName, message) => {
  connectActiveMQ(queueName, (client, destination) => {
    const sendHeaders = {
      'destination': destination,
      'content-type': 'text/plain'
    };

    const frame = client.send(sendHeaders);
    frame.write(JSON.stringify(message));
    frame.end();
    client.disconnect();
  });
};

// Initialiser des endpoints pour chaque service sender
Object.entries(services).forEach(([serviceName, queueName]) => {
  app.post(`/${serviceName}/send-notification`, (req, res) => {
    const { user, content } = req.body;
    const notification = { user, content, status: 'pending', serviceName };

    sendNotification(queueName, notification);

    res.status(200).json({ status: 'Notification sent', notification });
  });
});

app.post('/send-to-activemq', (req, res) => {
  const { queueName, message } = req.body; // Récupérer le nom de la queue et le message du corps de la requête
  if (!services[queueName]) {
    return res.status(404).json({ error: `Queue ${queueName} not found` });
  }

  // Utiliser la fonction existante pour envoyer le message
  sendNotification(services[queueName], message);

  res.status(200).json({ status: 'Message sent to ActiveMQ', message });
});

// Initialiser des consumers pour chaque service receiver
const startReceivers = () => {
  console.log('Starting receivers');
  Object.entries(services).forEach(([serviceName, queueName]) => {
    connectActiveMQ(queueName, (client, destination) => {
      const subscribeHeaders = {
        'destination': destination,
        'ack': 'client-individual'
      };

      client.subscribe(subscribeHeaders, (error, message) => {
        if (error) {
          console.log('Subscribe error ' + error.message);
          return;
        }
        
        message.readString('utf-8', (error, body) => {
          if (error) {
            console.log('Read message error ' + error.message);
            return;
          }
          try {
            const notification = JSON.parse(body);
            console.log(`[Receiver for ${serviceName}] Received notification:`, notification);

            // Emitting notification to all connected socket.io clients
            io.emit('notification', { service: serviceName, data: notification });

            // Handle notification according to the service logic
            client.ack(message);
          } catch (error) {
            console.log('Parse message error ' + error.message);
          }
        });
      });

      console.log(`Receiver for ${serviceName} started`);
    });
  });
};

// Gestion des connexions Socket.io
io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('disconnect', () => {
    console.log('User disconnected');
  });
});

// Démarrer le serveur et les receivers
const PORT = 3000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
  startReceivers();
});
