import { useEffect } from 'react';
import { io } from 'socket.io-client';


const UserNotification = () => {

  const socket = io('http://localhost:3001'); // Adjust the URL to match your Spring Boot application's WebSocket endpoint

  useEffect(() => {
    // user service 
    socket.on('user/notification', (notification) => {
      try {
          // let data = JSON.parse(notification)
          const { action, result ,payload } = notification;
          if (action==="create" && result==="progress") {
            alert("Opération en cours...");
          }
          if (action==="create" && result==="success") {
            alert("Compte crée avec succès !");
          }
          if (action==="error" && result==="failed") {
            alert("Echec de la création du compte !");
          }
      } catch (error) {
        alert(error.message)
      }
  
      console.log('Message reçu:', notification);
    });
    
    // Nettoyer en cas de démontage du composant
    return () => socket.off('user/notification');
  }, []);

  return;
};

export default UserNotification;
