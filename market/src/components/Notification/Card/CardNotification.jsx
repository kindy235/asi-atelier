import { useEffect } from 'react';
import { io } from 'socket.io-client';


const CardNotification = () => {

  const socket = io('http://localhost:3001'); // Adjust the URL to match your Spring Boot application's WebSocket endpoint

  useEffect(() => {
    // user service 
    socket.on('card/notification', (notification) => {
      try {
          // let data = JSON.parse(notification)
          const { action, result ,payload } = notification;
          if (action==="create" && result==="progress") {
            alert("Opération en cours...");
          }
          if (action==="create" && result==="success") {
            alert("Compte crée avec succès !");
          }
      } catch (error) {
        alert(error.message)
      }
  
      console.log('Message reçu:', notification);
    });
    
    // Nettoyer en cas de démontage du composant
    return () => socket.off('card/notification');
  }, []);

  return;
};

export default CardNotification;
