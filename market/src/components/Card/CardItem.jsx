import React from 'react';
import { Card, Icon, Image } from 'semantic-ui-react';

const CardItem = ({ card }) => {
  return (
    <Card>
      <div className="content">
        <div className="ui grid">
          <div className="three column row">
            <div className="column">
              <Icon name="heart outline" />
              <span id="cardHPId">{card.hp}</span>
            </div>
            <div className="column">
              <h5>Happy Tree Family</h5>
            </div>
            <div className="column">
              <span id="energyId">{card.energy}</span>
              <Icon name="lightning" />
            </div>
          </div>
        </div>
      </div>
      <div className="image imageCard">
        <div className="blurring dimmable image">
          <div className="ui inverted dimmer">
            <div className="content">
              <div className="center">
                <div className="ui primary button">Add Friend</div>
              </div>
            </div>
          </div>
          <Image fluid src={card.imgUrl} size='small' centered bordered circular />
        </div>
      </div>
      <div className="content">
        <div className="ui form tiny">
          <div className="field">
            <label id="cardNameId">{card.name}</label>
            <textarea
              id="cardDescriptionId"
              className="overflowHiden"
              readOnly
              rows="5"
            >
              {card.description}
            </textarea>
          </div>
        </div>
      </div>
      <div className="content">
        <Icon name="heart outline" />
        <span id="cardHPId"> HP {card.hp}</span>
        <div className="right floated">
          <span id="cardEnergyId">Energy {card.energy}</span>
          <Icon name="lightning" />
        </div>
      </div>
      <div className="content">
        <span className="right floated">
          <span id="cardAttackId"> Attack {card.attack}</span>
          <Icon name="wizard" />
        </span>
        <Icon name="protect" />
        <span id="cardDefenceId">Defense {card.defence}</span>
      </div>
      <div className="ui bottom attached button">
        <Icon name="money" />
        Actual Value <span id="cardPriceId">{card.price}$</span>
      </div>
    </Card>
  );
};

export default CardItem;
