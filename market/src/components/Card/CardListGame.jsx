import React, { useState } from 'react';
import { Grid, Header } from 'semantic-ui-react';
import CardItem from './CardItem'; // Make sure the path is correct

import { Image, Icon } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';


const CardListGame = ({cards, actionName}) => {
  const navigate = useNavigate(); 
  const onCardClick = (cardId) => {
    navigate(`/cards/${cardId}`);
  }

  return (
    <div>
      <Grid>
        <Grid.Column width={10}>
          <Header as="h3" className="ui aligned header">
            Selected Cards
          </Header>
          <table className="ui selectable celled table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Family</th>
                <th>HP</th>
                <th>Energy</th>
                <th>Defence</th>
                <th>Attack</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {cards.map((card, index) => (
                <tr key={index}>
                  <td onClick={() => onCardClick(card.id)} style={{ cursor: 'pointer' }}>
                    <Image avatar src={card.imgUrl}  /> {card.name}
                  </td>
                  <td>{card.description}</td>
                  <td>{card.family}</td>
                  <td>{card.hp}</td>
                  <td>{card.energy}</td>
                  <td>{card.defence}</td>
                  <td>{card.attack}</td>
                  <td>{card.price}$</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default CardListGame;
