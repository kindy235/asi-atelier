import { Grid, Header } from 'semantic-ui-react';
import CardItem from './CardItem'; // Make sure the path is correct
import React, { useEffect, useState } from 'react';

import { Image, Icon } from 'semantic-ui-react';
import { useDispatch, useSelector } from "react-redux";
import { selectCards, selectUser } from "../../core/selectors";
import { loadCards } from "../../core/actions";
import StoreNotification from "../Notification/Store/StoreNotification";
import { useNavigate } from 'react-router-dom';
import CardListGame from '../Card/CardListGame'; 
import { Input, Button, Form, Container } from 'semantic-ui-react';

const CardList = ({cards, actionName}) => {
      const navigate = useNavigate(); 
      cards = cards ? cards : [];
      const user = useSelector(selectUser);
      const [selectedCards, setSelectedCards] = useState([]); // State for selected cards
      const [isMaxReached, setIsMaxReached] = useState(false); // State to indicate if max limit is reached
      
      // useEffect(() => {
      //   const getCard = async () => {
            
      //   };
      //   getCard();
      // }, []);

      const doAction = (card)=>async () => {
        if (actionName.toLowerCase() == 'sell' || actionName.toLowerCase() == 'buy') {
          try {
            const response = await fetch(`http://localhost:8000/store-service/store/${actionName.toLowerCase()}`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                cardId: card.id,
                userId: user.id
              }),
            });
    
            if (response.ok) {
              const res = await response.text();
              alert(res);
            }
          } catch (error) {
            console.error('Erreur :', error.message);
          }
        }
        else {
          if (!selectedCards.some(selectedCard => selectedCard.id === card.id)) {
            if (selectedCards.length < 3) {
              // Add the card to selectedCards state if it's not already selected and not max limit reached
              setSelectedCards(prevSelectedCards => [...prevSelectedCards, card]);
              //alert(`Carte sélectionnée`);
              if (selectedCards.length === 2) {
                setIsMaxReached(true);
              }
            } else {
              setIsMaxReached(true);
              alert(`Maximum de cartes sélectionnées atteint !`);
            }
          } else {
            // Remove the card from selectedCards state if it's already selected
            setSelectedCards(prevSelectedCards => prevSelectedCards.filter(selectedCard => selectedCard.id !== card.id));
            //alert(`Carte désélectionnée`);
            setIsMaxReached(false); // Reset isMaxReached state when deselecting a card
          }
        }
      };

      const onCardClick = (cardId) => {
        navigate(`/cards/${cardId}`);
      }

      const createRoom = async () => {
        try {
          const response = await fetch('http://localhost:3200/createGame/', {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify ({
              playerId: user.id,
              playerCards: selectedCards
            })
          });
  
          if (response.ok) {
            const {gameCode} = await response.json();
            console.log(gameCode);
            navigate(`/room/${gameCode}`);
          }
        } catch (error) {
          console.error('Erreur :', error.message);
        }
      };

      
      const joinRandomRoom = async () => {
        
        try {
          const response = await fetch('http://localhost:3200/joinRandomGame/', {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify ({
              playerId: user.id,
              playerCards: selectedCards
            })
          });
  
          if (response.ok) {
            const {gameCode} = await response.json();
            console.log(gameCode);
            navigate(`/room/${gameCode}`);
          }
        } catch (error) {
          console.error('Erreur :', error.message);
        }
      };

      const joinRoom = async () => {
        
        try {
          const response = await fetch('http://localhost:3200/joinGame/', {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify ({
              gameCode: document.getElementById("coderoom").value,
              playerId: user.id,
              playerCards: selectedCards
            })
          });
  
          if (response.ok) {
            navigate(`/room/${document.getElementById("coderoom").value}`);
          }
        } catch (error) {
          console.error('Erreur :', error.message);
        }
      };

  return (
    <div style={{margin: "0 3%"}}>
    <Grid>
      <Grid.Column width={10}>
        <Header as="h3" className="ui aligned header">
          My Cards
        </Header>
        <table className="ui selectable celled table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Family</th>
              <th>HP</th>
              <th>Energy</th>
              <th>Defence</th>
              <th>Attack</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cards.map((card, index) => (
              <tr key={index}>
                <td onClick={() => onCardClick(card.id)} style={{ cursor: 'pointer' }}>
                  <Image avatar src={card.imgUrl}  /> {card.name}
                </td>
                <td>{card.description}</td>
                <td>{card.family}</td>
                <td>{card.hp}</td>
                <td>{card.energy}</td>
                <td>{card.defence}</td>
                <td>{card.attack}</td>
                <td>{card.price}$</td>
                <td>
                  <div className="ui vertical animated button" onClick={doAction(card)} tabIndex="0">
                    <div className="hidden content">{actionName}</div>
                    <div className="visible content">
                    {actionName.toLowerCase() === 'select' ? (
                      <Icon name="play" />
                    ) : <Icon name="shop" />}
                    </div>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Grid.Column>
    </Grid>
    {actionName.toLowerCase() === 'select' ? (
      <>
          <CardListGame cards={selectedCards} actionName={"unselect"} />
          <br/>
          {isMaxReached ? (
            <div>
              <Button type="button" onClick={createRoom}>Create room</Button>
              <Button type="button" onClick={joinRandomRoom}>Join random room</Button>
              <Button type="button" onClick={joinRoom}>Join room with room code</Button>
              <Input type="text" id="coderoom" placeholder="room code"></Input>
              </div>
            ) : 
              <div>
                <Button disabled type="button">Create room</Button>
                <Button disabled type="button">Join random room</Button>
                <Button disabled type="button">Join room with room code</Button>
              </div>
            }

          
      </>
      ) : null}
     
    </div>
  );
};

export default CardList;
