import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'; // Import useParams from react-router-dom
import CardItem from './CardItem'; // Make sure the path is correct
import { useSelector } from "react-redux";
import { selectUser } from "../../core/selectors";
import Header from '../Home/Header'; 


const CardDetails = () => {
    const { id } = useParams(); // Capture the model ID from the URL
    const [card, setCard] = useState(null); // Use array destructuring for useState

    useEffect(() => {
        const getCard = async () => {
            try {
                const response = await fetch(`http://localhost:8000/card-service/cards/${id}`);

                if (response.ok) {
                    const data = await response.json();
                    setCard(data);
                } else {
                    throw new Error('Card not found');
                }
            } catch (error) {
                console.error('Error:', error.message);
            }
        };

        const getCrdRef = async () => {
            try {
                const response = await fetch(`http://localhost:8000/card-service/cards/ref/${id}`);

                if (response.ok) {
                    const data = await response.json();
                    setCard(data);
                } else {
                    throw new Error('Card not found');
                }
            } catch (error) {
                console.error('Error:', error.message);
            }
        };

        getCard();
        getCrdRef();
    }, [id]); // Include id in the dependency array to fetch data when id changes
    const user = useSelector(selectUser);
    return (
        <div>
            <Header user={user} title={"Card Details"} desc={"Show a card details"} />
            <div style={{ margin: "0 3%" }}>
            {card ? (
                <CardItem card={card} />
            ) : (
                <p>Card not found!</p>
            )}
        </div>
        </div>
    );
};

export default CardDetails;
