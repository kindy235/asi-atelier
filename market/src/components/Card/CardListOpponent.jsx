import React, { useState } from 'react';
import { Grid, Header } from 'semantic-ui-react';
import CardItem from './CardItem'; // Make sure the path is correct

import { Image, Icon } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';


const CardListOpponent = ({cards, actionName}) => {
  const navigate = useNavigate(); 
  const onCardClick = (cardId) => {
    navigate(`/cards/${cardId}`);
  }

  return (
    <div>
      <Grid>
        <Grid.Column width={10}>
          <table className="ui selectable celled table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Energy</th>
                <th>ID</th>
              </tr>
            </thead>
            <tbody>
              {cards.map((card, index) => (
                <tr key={index} style={{ backgroundColor: card.energy === 0 ? '#FFCCCC' : 'inherit' }}>
                  <td onClick={() => onCardClick(card.id)} style={{ cursor: 'pointer' }}>
                    <Image avatar src={card.imgUrl}  /> {card.name}
                  </td>
                  <td>{card.energy}</td>
                  <td>{card.id}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Grid.Column>
      </Grid>
    </div>
  );
};

export default CardListOpponent;
