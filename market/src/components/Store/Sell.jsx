import Header from '../Home/Header'; 
import CardList from '../Card/CardList'; 
import { useSelector } from "react-redux";
import { selectUser } from "../../core/selectors";


const Sell = () => {

  const user = useSelector(selectUser);

  return (
    <div>
    <Header user={user} title={"SELL"} desc={"Sell your card to get money"} />
      <CardList cards={user.cardList} actionName={"Sell"}></CardList>
    </div>
  );
};

export default Sell;
