import React, { useEffect } from 'react';
import Header from '../Home/Header'; 
import CardList from '../Card/CardList'; 
import { useDispatch, useSelector } from "react-redux";
import { selectRefCards, selectUser } from "../../core/selectors";
import { loadRefCards, loadUserCards } from "../../core/actions";

const Buy = () => {
  
  const dispatch = useDispatch()

  useEffect(() => {
    const getCardsRef = async () => {
      try {
        const response = await fetch('http://localhost:8000/card-service/cards/ref');

        if (response.ok) {
          const cardsRef = await response.json();
          dispatch(loadRefCards(cardsRef));
        }
      } catch (error) {
        console.error('Erreur :', error.message);
      }
    };
    getCardsRef();
  }, [dispatch]);

  const cards = useSelector(selectRefCards);
  const user = useSelector(selectUser);

  return (
    <div >
    <Header user={user} title={"BUY"} desc={"Buy a card and collect them"} />
      <CardList cards={cards} actionName={"Buy"}></CardList>
    </div>
  );
};



export default Buy;
