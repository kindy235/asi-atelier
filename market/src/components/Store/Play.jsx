import Header from '../Home/Header';
import { useSelector } from "react-redux";
import { selectUser } from "../../core/selectors";
import CardList from '../Card/CardList'; 

const Play = () => {
  let user = useSelector(selectUser)
  return (
    <div>
    <Header user={user} title={"PLAY"} desc={"Join a room and play with someone"} />
      <CardList cards={user.cardList} actionName={"Select"}></CardList>
    </div>
  );
};

export default Play;
