import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Cookies from 'js-cookie';
import Login from './components/Authentification/Login'; 
import Home from './components/Home/Home'; 
import Buy from './components/Store/Buy';
import Sell from './components/Store/Sell';
import Play from './components/Store/Play';
import Room from './components/Game/Room';
import CardDetails from './components/Card/CardDetails';
import { useDispatch, useSelector } from "react-redux";
import { loadUser } from "./core/actions";
import { selectUser } from "./core/selectors";


function App() {

  const dispatch = useDispatch();
   // Vérifier la connexion au chargement de la page
   useEffect(() => {
    const checkLoginStatus = async () => {
      try {
        const response = await fetch('/auth-service/auth/check-auth', {
          method: 'POST',
          credentials: 'include',
        });

        if (response.ok) {
          const userData = await response.json();
          dispatch(loadUser(userData));
          
        }
      } catch (error) {
        console.error('Erreur lors de la vérification de la connexion :', error.message);
      }
    };
    checkLoginStatus();
  }, [dispatch]);

  let user_id = Cookies.get('userId');
  let user = useSelector(selectUser);
  console.log('userId :', user_id)
  console.log('user :', user)
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={user ? <Home/> : <Login/>} />
          <Route path="/buy" element={user ? <Buy/> : <Login  />} />
          <Route path="/sell" element={user ? <Sell/> : <Login  />}/>
          <Route path="/play" element={user ? <Play/> : <Login  />} />
          <Route path="/cards/:id" element={user ? <CardDetails/> : <Login  />} />
          <Route path="/room/:roomcode" element={user ? <Room/> : <Login  />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
