const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const cors = require('cors');
const app = express();
app.use(cors());
const server = http.createServer(app);
//const io = socketIo(server);

const io = socketIo(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

const roomUserCount = {};

io.on('connection', (socket) => {
  socket.on('join room', ({ room, name }) => {
    const userCount = roomUserCount[room] || 0;

    if (userCount < 2) {
      socket.join(room);
      roomUserCount[room] = (roomUserCount[room] || 0) + 1;
      console.log(`${name} a rejoint la salle ${room}`);
      socket.to(room).emit('user joined', `${name} a rejoint la salle`);
    } else {
      const nextRoom = parseInt(room, 10) + 1;
      socket.emit('room full', { currentRoom: room, nextRoom: nextRoom.toString() });
    }
  });

  socket.on('chat message', ({ room, name, message }) => {
    io.to(room).emit('chat message', { name, message });
  });

  socket.on('disconnect', () => {
    // Gérer ici la logique de déconnexion
    console.log('Un utilisateur déconnecté');
  });
});

server.listen(3000, () => {
  console.log('Écoute sur *:3000');
});
