import React, { useState, useEffect, useCallback } from 'react';
import io from 'socket.io-client';

const socket = io('http://localhost:3000');

function App() {
  const [room, setRoom] = useState('');
  const [name, setName] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const [hasJoined, setHasJoined] = useState(false);

  useEffect(() => {
    fetch('http://localhost:8082/user/1')
      .then(response => response.json())
      .then(data => {
        setName(data.username);  
        setHasJoined(true);      
      })
      .catch(error => console.error('Erreur lors de la récupération des informations utilisateur:', error));
  }, []);

  const handleJoinRoom = useCallback(() => {
    if (room && name) {
      socket.emit('join room', { room, name });
      setHasJoined(true);
    }
  }, [room, name]);

  const handleSendMessage = useCallback((e) => {
    e.preventDefault();
    if (message) {
      socket.emit('chat message', { room, name, message });
      setMessage('');
    }
  }, [room, name, message]);

  useEffect(() => {
    socket.on('room full', ({ nextRoom }) => {
      setRoom(nextRoom);  // Mise à jour automatique de la salle
      handleJoinRoom();   // Tentative de rejoindre la nouvelle salle
    });

    socket.on('user joined', (msg) => {
      setMessages(messages => [...messages, { type: 'system', message: msg }]);
    });

    socket.on('chat message', ({ name, message }) => {
      setMessages(messages => [...messages, { type: 'message', name, message }]);
    });

    return () => {
      socket.off('room full');
      socket.off('user joined');
      socket.off('chat message');
    };
  }, [handleJoinRoom]);

  return (
    <div>
      <h1>Chat en temps réel</h1>
      {!hasJoined ? (
        <div>
          <input
            type="text"
            placeholder="Room"
            value={room}
            onChange={(e) => setRoom(e.target.value)}
          />
          <button onClick={handleJoinRoom}>Join Room</button>
        </div>
      ) : (
        <div>
          <h2>Salle: {room}</h2>
          <h3>Bienvenue, {name}</h3>
          <ul>
            {messages.map((msg, index) => (
              <li key={index}>
                {msg.type === 'system' ? (
                  <span>{msg.message}</span>
                ) : (
                  <span>
                    <strong>{msg.name}:</strong> {msg.message}
                  </span>
                )}
              </li>
            ))}
          </ul>
          <form onSubmit={handleSendMessage}>
            <input
              type="text"
              value={message}
              onChange={(e) => setMessage(e.target.value)}
            />
            <button type="submit">Send</button>
          </form>
        </div>
      )}
    </div>
  );
}

export default App;
