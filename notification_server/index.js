const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
  });


app.get('/', (req, res) => {
  res.send('Notification Server is running!');
});

// Handle POST requests
app.post('/user/notify', (req, res) => {
  const receivedData = req.body;
  console.log(`Received data from ${req.baseUrl} :`, receivedData, );
  res.send('Data received successfully'); // Send a response back to the client

  // Send data to the connected client
  io.emit('user/notification', receivedData);

});

// Handle POST requests
app.post('/card/notify', (req, res) => {
  const receivedData = req.body;
  console.log(`Received data from ${req.baseUrl} :`, receivedData, );
  res.send('Data received successfully'); // Send a response back to the client

  // Send data to the connected client
  io.emit('card/notification', receivedData);

});

// Handle POST requests
app.post('/store/notify', (req, res) => {
    const receivedData = req.body;
    console.log(`Received data from ${req.baseUrl} :`, receivedData, );
    res.send('Data received successfully'); // Send a response back to the client

    // Send data to the connected client
    io.emit('store/notification', receivedData);

});

io.on('connection', (socket) => {
    console.log('A user connected');
      
    // Listen for client events
    socket.on('messageFromClient', (data) => {
        console.log('Message from client:', data);
    });

    socket.on('disconnect', () => {
    console.log('User disconnected');
  });
});


const PORT = process.env.PORT || 3001;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
