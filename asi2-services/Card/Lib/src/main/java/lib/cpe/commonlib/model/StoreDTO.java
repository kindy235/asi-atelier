package lib.cpe.commonlib.model;

import java.sql.Timestamp;

public class StoreDTO {

    private Integer id;
    private Integer userId;
    private Integer cardId;
    private Integer price;
    private String action;
    private Timestamp timeSt;

    public StoreDTO() {
        this.timeSt = new Timestamp(System.currentTimeMillis());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Timestamp getTimeSt() {
        return timeSt;
    }

    public void setTimeSt(Timestamp timeSt) {
        this.timeSt = timeSt;
    }
}
