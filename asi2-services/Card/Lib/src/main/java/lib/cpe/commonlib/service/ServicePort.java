package lib.cpe.commonlib.service;

public enum ServicePort {
    USER_SERVICE(8082),
    AUTH_SERVICE(8083),
    CARD_SERVICE(8084),
    STORE_SERVICE(8085);

    private final int port;

    ServicePort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }
}
