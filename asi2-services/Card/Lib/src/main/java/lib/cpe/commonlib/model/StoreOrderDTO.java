package lib.cpe.commonlib.model;

public class StoreOrderDTO {

    private Integer userId;
    private Integer cardId;

    public StoreOrderDTO() {
        // Default constructor
    }

    public StoreOrderDTO(Integer userId, Integer cardId) {
        this.userId = userId;
        this.cardId = cardId;
    }

    // Getters and Setters for each field

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }
}
