package com.cpe.springboot.card.Controller;

import com.cpe.springboot.card.model.CardReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lib.cpe.commonlib.model.CardDTO;
import com.cpe.springboot.card.service.CardModelService;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.utils.DTOMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/cards")
public class CardRestController {

	private final CardModelService cardModelService;

	public CardRestController(CardModelService cardModelService) {
		this.cardModelService = cardModelService;
	}

	@GetMapping("/all")
	public List<CardDTO> getAllCards() {
		return DTOMapper.fromCardModelListToCardDTOList(cardModelService.getAllCardModel());
	}

	@GetMapping("/ref")
	public List<CardDTO> getAllCardsRef() {
		return DTOMapper.fromCardMRefListToCardDTOList(cardModelService.getAllCardRef());
	}

	@GetMapping("/{id}")
	public ResponseEntity<CardDTO> getCard(@PathVariable Integer id) {
		CardModel cardModel = cardModelService.getCard(id)
				.orElseThrow(() -> new CardNotFoundException("Card id: " + id + ", not found"));

		return ResponseEntity.ok(DTOMapper.fromCardModelToCardDTO(cardModel));
	}
	@GetMapping("/UserCards/{userId}")
	public ResponseEntity<List<CardDTO>> getCardByUserId(@PathVariable Integer userId) {
		List<CardModel> cardModelList = cardModelService.getCardsUser(userId);
		if(cardModelList.isEmpty())
			throw new CardNotFoundException("User id: " + userId + ", not found");

		return ResponseEntity.ok(DTOMapper.fromCardModelListToCardDTOList(cardModelList));
	}

	@GetMapping("/ref/{id}")
	public ResponseEntity<CardDTO> getCardRefById(@PathVariable Integer id) {
		CardReference cardReference = cardModelService.getRefCard(id)
				.orElseThrow(() -> new CardNotFoundException("Card id: " + id + ", not found"));

		return ResponseEntity.ok(DTOMapper.fromCardRefToCardDTO(cardReference));
	}

	@PostMapping
	public ResponseEntity<String> addCard(@RequestBody CardDTO cardDTO) {
		cardModelService.actionAsync(DTOMapper.fromCardDtoToCardModel(cardDTO), "create");
		return ResponseEntity.ok("Add card operation is in progress");
	}

	@PutMapping("/{id}")
	public ResponseEntity<String> updateCard(@PathVariable Integer id, @RequestBody CardDTO cardDTO) {
		if (!id.equals(cardDTO.getId())) {
			return ResponseEntity.badRequest().build();
		}
		cardModelService.actionAsync(DTOMapper.fromCardDtoToCardModel(cardDTO), "update");
		return ResponseEntity.ok("Update card operation is in progress");
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteCard(@PathVariable Integer id) {
		CardModel optCard = cardModelService.getCard(id)
				.orElseThrow(() -> new CardNotFoundException("Card id: " + id + ", not found"));

		cardModelService.actionAsync(optCard, "delete");
		return ResponseEntity.ok("Delete card operation is in progress");
	}

	@GetMapping("/to_sell")
	public List<CardDTO> getCardsToSell() {
		return cardModelService.getAllCardToSell().get().stream()
				.map(DTOMapper::fromCardModelToCardDTO)
				.collect(Collectors.toList());
	}

	@GetMapping("/random/{userId}")
	public List<CardDTO> getRandCards(@PathVariable Integer userId) {
		return DTOMapper.fromCardModelListToCardDTOList(cardModelService.getRandCard(userId));
	}

	@ExceptionHandler(CardNotFoundException.class)
	public ResponseEntity<String> handleCardNotFoundException(CardNotFoundException e) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	}
	private static class CardNotFoundException extends RuntimeException {
		public CardNotFoundException(String message) {
			super(message);
		}
	}
}
