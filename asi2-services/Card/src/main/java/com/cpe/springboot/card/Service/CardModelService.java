package com.cpe.springboot.card.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;


import com.cpe.springboot.card.utils.DTOMapper;
import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgemitter.BusService;
import com.cpe.springboot.esb.notification.NotificationService;
import lib.cpe.commonlib.model.CardDTO;
import lib.cpe.commonlib.model.UserDTO;
import lib.cpe.commonlib.service.UserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.repository.CardModelRepository;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.model.CardModel;

@Service
public class CardModelService {
	private final CardModelRepository cardRepository;
	private final CardReferenceService cardRefService;
	private final UserApi userApi;
	private Random rand;
	@Autowired
	private BusService<Notification> busService;
	@Autowired
	NotificationService notificationService;

	public CardModelService(CardModelRepository cardRepository,CardReferenceService cardRefService) {
		this.rand=new Random();
		// Dependencies injection by constructor
		this.cardRepository=cardRepository;
		this.cardRefService=cardRefService;
		this.userApi = new UserApi();
	}
	
	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public List<CardReference> getAllCardRef() {
		return new ArrayList<>(cardRefService.getAllCardRef());
	}

	public List<CardModel> getCardsUser(Integer userId){
		List<CardModel> cardList = new ArrayList<>();
		Optional<List<CardModel>> cardModelList = cardRepository.findAllByUserId(userId);
        cardModelList.ifPresent(cardList::addAll);
		return cardList;
	}

	public CardModel addCard(CardModel cardModel) {
		return cardRepository.save(cardModel);
	}

	public void updateCardRef(CardModel cardModel) {
		cardRepository.save(cardModel);

	}
	public CardModel updateCard(CardModel updatedCardModel) {
		// Récupérez la carte existante de la base de données
		Optional<CardModel> optionalCardModel = cardRepository.findById(updatedCardModel.getId());
		
		if (optionalCardModel.isPresent()) {
			CardModel existingCardModel = optionalCardModel.get();
			
			// Mettez à jour uniquement les champs modifiés
			if (updatedCardModel.getAttack() != null) {
				existingCardModel.setAttack(updatedCardModel.getAttack());
			}
			
			if (updatedCardModel.getDefence() != null) {
				existingCardModel.setDefence(updatedCardModel.getDefence());
			}
			
			if (updatedCardModel.getEnergy() != null) {
				existingCardModel.setEnergy(updatedCardModel.getEnergy());
			}
			
			if (updatedCardModel.getHp() != null) {
				existingCardModel.setHp(updatedCardModel.getHp());
			}
			
			if (updatedCardModel.getPrice() != null) {
				existingCardModel.setPrice(updatedCardModel.getPrice());
			}

			existingCardModel.setUserId(updatedCardModel.getUserId());
			
			// Enregistrez les modifications dans la base de données

			// Convertissez le modèle mis à jour en DTO et renvoyez-le
			return cardRepository.save(existingCardModel);
		}
		
		// Si la carte n'existe pas, vous pouvez renvoyer une valeur par défaut ou générer une exception, selon votre cas d'utilisation.
		return null;
	}
	public Optional<CardModel> getCard(Integer id) {
		return cardRepository.findById(id);
	}

	public Optional<CardReference> getRefCard(Integer id) {
		return cardRefService.findById(id);
	}

	public void deleteCardModel(Integer id) {
		cardRepository.deleteById(id);
	}
	
	public List<CardModel> getRandCard(Integer userId){
		List<CardModel> cardList=new ArrayList<>();
		//UserDTO user = userApi.getUserById(userId);

		for(int i=0;i<5;i++) {
			CardReference currentCardRef=cardRefService.getRandCardRef();
			CardModel currentCard=new CardModel(currentCardRef);
			currentCard.setUserId(userId);
			currentCard.setAttack(rand.nextFloat()*100);
			currentCard.setDefence(rand.nextFloat()*100);
			currentCard.setEnergy(100f);
			currentCard.setHp(rand.nextFloat()*100);
			currentCard.setPrice(currentCard.computePrice());
			//save new card before sending for user creation
			//this.addCard(currentCard);
			cardList.add(currentCard);
		}
		return (List<CardModel>) cardRepository.saveAll(cardList);
	}

	public Optional<List<CardModel>> getAllCardToSell() {
		return this.cardRepository.findAllByUserId(null);
	}

	public void actionAsync(CardModel card, String actionName){
		CompletableFuture.runAsync(() -> {
			notificationService.sendNotification("card/notify", new Notification<>(actionName, "progress", null));
			busService.sendMsg(new Notification<>(actionName, "progress", card), "CARD_SERVICE_BUS");
		});
	}
}

