package com.cpe.springboot.card.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.repository.CardRefRepository;
@Service
public class CardReferenceService {
	
	private final CardRefRepository cardRefRepository;
	
	public CardReferenceService(CardRefRepository cardRefRepository) {
		this.cardRefRepository=cardRefRepository;
	}

	public List<CardReference> getAllCardRef() {
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public void addCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);
	}

	public Optional<CardReference> findById(Integer id) {
		return cardRefRepository.findById(id);
	}

	public CardReference getRandCardRef() {
		List<CardReference> cardRefList=getAllCardRef();
		if( cardRefList.size()>0) {
			Random rand=new Random();
			int rindex=rand.nextInt(cardRefList.size()-1);
			return cardRefList.get(rindex);
		}
		return null;
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {

		if (getAllCardRef().size() == 0){
			addCardRef(new CardReference(
					"Superman",
					"Superhero from DC Comics",
					"DC",
					"Fire",
					"https://vignette.wikia.nocookie.net/lego/images/4/48/76096_Minifigure_04.jpg/revision/latest/scale-to-width-down/250?cb=20190729133554",
					"https://vignette.wikia.nocookie.net/lego/images/4/48/76096_Minifigure_04.jpg/revision/latest/scale-to-width-down/250?cb=20190729133554"
			));

			addCardRef(new CardReference(
					"Batman",
					"Superhero from DC Comics",
					"DC",
					"Water",
					"https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg",
					"https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg"
			));

			addCardRef(new CardReference(
					"DeadPool",
					"Superhero from Marvel Comics",
					"MARVEL",
					"Fire",
					"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/018b1215-92bb-49ca-bfc3-67afc7a64a3f/d6alhm4-bc82178e-ceb8-44b8-b484-9e35b933ef48.png/v1/fill/w_1070,h_738/deadpool_icon___png_by_axeswy_d6alhm4-fullview.png",
					"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/018b1215-92bb-49ca-bfc3-67afc7a64a3f/d6alhm4-bc82178e-ceb8-44b8-b484-9e35b933ef48.png/v1/fill/w_1070,h_738/deadpool_icon___png_by_axeswy_d6alhm4-fullview.png"
			));

			addCardRef(new CardReference(
					"Hulk",
					"Superhero from DC Marvel",
					"MARVEL",
					"Earth",
					"https://icon-library.com/images/2018/2620922_hulk-avengers-hulk-png-transparent-png.png",
					"https://icon-library.com/images/2018/2620922_hulk-avengers-hulk-png-transparent-png.png"
			));

			addCardRef(new CardReference(
					"Spider-Man",
					"Superhero from DC Marvel",
					"MARVEL",
					"Earth",
					"https://icon-library.com/images/icon-spiderman/icon-spiderman-28.jpg",
					"https://icon-library.com/images/icon-spiderman/icon-spiderman-28.jpg"
			));
		}
	}
}
