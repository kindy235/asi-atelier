package com.cpe.springboot.esb.msgreceiver;

import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

@Component
public interface BusListener {
    void recvMsg(TextMessage message);
}
