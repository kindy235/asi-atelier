package com.cpe.springboot.card.utils;

import com.cpe.springboot.card.model.CardReference;
import lib.cpe.commonlib.model.CardDTO;
import com.cpe.springboot.card.model.CardModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DTOMapper {
    public static CardDTO fromCardModelToCardDTO(CardModel cardModel) {
        CardDTO cardDTO = new CardDTO();
        cardDTO.setId(cardModel.getId());
        cardDTO.setAttack(cardModel.getAttack());
        cardDTO.setDefence(cardModel.getDefence());
        cardDTO.setEnergy(cardModel.getEnergy());
        cardDTO.setHp(cardModel.getHp());
        cardDTO.setPrice(cardModel.getPrice());
        cardDTO.setUserId(cardModel.getUserId());
        cardDTO.setName(cardModel.getName());
        cardDTO.setDescription(cardModel.getDescription());
        cardDTO.setImgUrl(cardModel.getImgUrl());
        cardDTO.setSmallImgUrl(cardModel.getSmallImgUrl());
        cardDTO.setFamily(cardModel.getFamily());
        cardDTO.setAffinity(cardModel.getAffinity());
        return cardDTO;
    }

    public static CardDTO fromCardRefToCardDTO(CardReference cardRef) {
        Random rand = new Random();
        CardDTO cardDTO = new CardDTO();
        float attack = rand.nextFloat()*100;
        float defence = rand.nextFloat()*100;
        float energy = 100f;
        float hp = rand.nextFloat()*100;

        cardDTO.setId(cardRef.getId());
        cardDTO.setAttack(attack);
        cardDTO.setDefence(defence);
        cardDTO.setEnergy(100f);
        cardDTO.setHp(hp);
        cardDTO.setPrice(hp * 20 + defence*20 + energy*20 + attack*20);

        cardDTO.setName(cardRef.getName());
        cardDTO.setDescription(cardRef.getDescription());
        cardDTO.setImgUrl(cardRef.getImgUrl());
        cardDTO.setSmallImgUrl(cardRef.getSmallImgUrl());
        cardDTO.setFamily(cardRef.getFamily());
        cardDTO.setAffinity(cardRef.getAffinity());
        return cardDTO;
    }

    public static List<CardDTO> fromCardModelListToCardDTOList(List<CardModel> cardModel) {
        List<CardDTO> cardDTOS = new ArrayList<>();
        for(CardModel card : cardModel)
        {
            cardDTOS.add(fromCardModelToCardDTO(card));
        }

        return cardDTOS;
    }

    public static List<CardDTO> fromCardMRefListToCardDTOList(List<CardReference> cardRefList) {
        List<CardDTO> cardDTOS = new ArrayList<>();
        for(CardReference card : cardRefList)
        {
            cardDTOS.add(fromCardRefToCardDTO(card));
        }

        return cardDTOS;
    }

    public static CardModel fromCardDtoToCardModel(CardDTO cardDTO) {
        CardModel cardModel = new CardModel();
        cardModel.setId(cardDTO.getId());
        cardModel.setAttack(cardDTO.getAttack());
        cardModel.setDefence(cardDTO.getDefence());
        cardModel.setEnergy(cardDTO.getEnergy());
        cardModel.setHp(cardDTO.getHp());
        cardModel.setPrice(cardDTO.getPrice());
        cardModel.setUserId(cardDTO.getUserId());

        cardModel.setName(cardDTO.getName());
        cardModel.setDescription(cardDTO.getDescription());
        cardModel.setImgUrl(cardDTO.getImgUrl());
        cardModel.setSmallImgUrl(cardDTO.getSmallImgUrl());
        cardModel.setFamily(cardDTO.getFamily());
        cardModel.setAffinity(cardDTO.getAffinity());
        return cardModel;
    }
}