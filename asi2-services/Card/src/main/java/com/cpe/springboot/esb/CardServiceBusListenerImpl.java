package com.cpe.springboot.esb;


import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.service.CardModelService;
import com.cpe.springboot.card.utils.DTOMapper;
import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgreceiver.BusListener;
import com.cpe.springboot.esb.notification.NotificationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

@Component
public class CardServiceBusListenerImpl implements BusListener {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CardModelService cardService;

    @Autowired
    private NotificationService notificationService;

    private void doReceive(String busName, TextMessage message) {
        try {
            String clazz = message.getStringProperty("ObjectType");
            Notification<CardModel> o = (Notification<CardModel>) objectMapper.readValue(message.getText(), Class.forName(clazz));

            CardModel card = objectMapper.convertValue(o.getPayload(), CardModel.class);
            String actionName = o.getAction();
            if (actionName.equals("create")) {
                CardModel newCard = cardService.addCard(card);
                notificationService.sendNotification("card/notify", new Notification<>(actionName, "success" , DTOMapper.fromCardModelToCardDTO(newCard)));
            }
            if (actionName.equals("update")) {
                CardModel updatedCard = cardService.updateCard(card);
                notificationService.sendNotification("card/notify", new Notification<>(actionName, "success", DTOMapper.fromCardModelToCardDTO(updatedCard)));
            }
            if (actionName.equals("delete")) {
                cardService.deleteCardModel(card.getId());
                notificationService.sendNotification("card/notify", new Notification<>(actionName, "success", null));
            }
            System.out.println("[BUSLISTENER] [CHANNEL "+busName+"] RECEIVED String MSG=["+message.getText()+"]");

        } catch (Exception e) {
            notificationService.sendNotification("card/notify", new Notification<>("error", "failed", null));
        }
    }

    @JmsListener(destination = "CARD_SERVICE_BUS", containerFactory = "queueConnectionFactory")
    public void recvMsg(TextMessage message) {
        doReceive("CARD_SERVICE_BUS", message);
    }

}
