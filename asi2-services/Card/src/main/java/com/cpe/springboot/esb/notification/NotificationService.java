package com.cpe.springboot.esb.notification;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NotificationService {

    private final String baseUrl = "http://notify-service:3001/"; // Replace with your Node.js server URL
    private final RestTemplate restTemplate;


    public NotificationService() {
        this.restTemplate = new RestTemplate();
    }

    public <T> void sendNotification(String endpoint, T data) {
        try {
            String url = baseUrl + endpoint;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<T> requestEntity = new HttpEntity<>(data, headers);

            ResponseEntity<String> response = restTemplate.postForEntity(
                    url,
                    requestEntity,
                    String.class
            );
            if (response.getStatusCode() == HttpStatus.OK){
                System.out.println("[data]"+ data +" successfully sent to NOTIFICATION SERVER");
            }else {
                System.out.println("send to NOTIFICATION SERVER failed !");
            }
        }catch (Exception ignored){}
    }
}

