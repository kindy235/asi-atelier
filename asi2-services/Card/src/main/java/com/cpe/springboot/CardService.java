package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@EnableJms
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Card Rest Api", version = "1.0", description = "Information about the Card APi and how to interact with"))
public class CardService {
	public static void main(String[] args) {
		SpringApplication.run(CardService.class, args);
	}

}
