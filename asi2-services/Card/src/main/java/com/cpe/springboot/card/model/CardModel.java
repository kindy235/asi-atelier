package com.cpe.springboot.card.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lib.cpe.commonlib.model.StoreDTO;
import lib.cpe.commonlib.model.UserDTO;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardModel extends CardBasics{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Float energy;
	private Float hp;
	private Float defence;
	private Float attack;
	private Float price;

	private Integer userId;
	private Integer storeId;

	public CardModel() {
		super();
	}
	
	public CardModel( CardModel cModel) {
		super(cModel);
		this.energy=cModel.getEnergy();
		this.hp=cModel.getHp();
		this.defence=cModel.getDefence();
		this.attack=cModel.getAttack();
		this.price=cModel.getPrice();
	}

	public CardModel( CardBasics cardBasic) {
		super(cardBasic);
	}

	public CardModel(String name, String description, String family, String affinity, Float energy, Float hp,
					 Float defence, Float attack,String imgUrl,String smallImg,Float price) {
		super(name, description, family, affinity,imgUrl,smallImg);
		this.energy = energy;
		this.hp = hp;
		this.defence = defence;
		this.attack = attack;
		this.price=price;
		this.price=this.computePrice();
	}
	public Float getEnergy() {
		return energy;
	}
	public void setEnergy(Float energy) {
		this.energy = energy;
	}
	public Float getHp() {
		return hp;
	}
	public void setHp(Float hp) {
		this.hp = hp;
	}
	public Float getDefence() {
		return defence;
	}
	public void setDefence(Float defence) {
		this.defence = defence;
	}
	public Float getAttack() {
		return attack;
	}
	public void setAttack(Float attack) {
		this.attack = attack;
	}

	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}



	public Float computePrice() {
		return this.hp * 20 + this.defence*20 + this.energy*20 + this.attack*20;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
}
