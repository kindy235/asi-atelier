package com.cpe.springboot.card.repository;

import com.cpe.springboot.card.model.CardModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    Optional<CardModel> findById(Integer id);
    Optional<List<CardModel>> findAllByUserId(Integer userId);
    Optional<List<CardModel>> findAllByPrice(Float price);
}
