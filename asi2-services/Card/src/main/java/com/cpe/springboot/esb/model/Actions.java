package com.cpe.springboot.esb.model;

public enum Actions {
    CREATE,
    UPDATE,
    DELETE
}
