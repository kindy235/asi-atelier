package com.cpe.springboot.esb.model;

public class Notification<T> {
    private String action;

    private String result;
    private T payload;

    public Notification(String action, String result, T payload) {
        this.action = action;
        this.result = result;
        this.payload = payload;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
