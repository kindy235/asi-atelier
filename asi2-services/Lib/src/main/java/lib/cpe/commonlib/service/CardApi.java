package lib.cpe.commonlib.service;

import lib.cpe.commonlib.model.CardDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class CardApi {
    private final String BASE_URL = "http://card-service:8084/cards/";
    private final RestTemplate restTemplate;

    public CardApi() {
        this.restTemplate = new RestTemplate();
    }

    public List<CardDTO> getAllCards() {
        ResponseEntity<CardDTO[]> response = restTemplate.getForEntity(BASE_URL, CardDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public CardDTO getCardById(Integer id) {
        ResponseEntity<CardDTO> response = restTemplate.getForEntity(BASE_URL + id, CardDTO.class);
        return response.getBody();
    }

    public CardDTO getCardRefById(Integer id) {
        ResponseEntity<CardDTO> response = restTemplate.getForEntity(BASE_URL + "ref/" + id, CardDTO.class);
        return response.getBody();
    }

    public List<CardDTO> getCardByUserId(Integer userId){
        ResponseEntity<CardDTO[]> response = restTemplate.getForEntity(BASE_URL + "UserCards/" + userId, CardDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public void addCard(CardDTO cardDTO) {
        restTemplate.postForEntity(BASE_URL, cardDTO, String.class);
    }

    public void updateCard(CardDTO cardDTO) {
        restTemplate.put(BASE_URL + cardDTO.getId(), cardDTO);
    }

    public void deleteCard(Integer id) {
        restTemplate.delete(BASE_URL + id);
    }

    public List<CardDTO> getCardsToSell() {
        ResponseEntity<CardDTO[]> response = restTemplate.getForEntity(BASE_URL + "to_sell", CardDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public List<CardDTO> getRandCards(Integer userId) {
        ResponseEntity<CardDTO[]> response = restTemplate.getForEntity(BASE_URL + "random/" + userId, CardDTO[].class);
        return Arrays.asList(response.getBody());
    }
}
