package lib.cpe.commonlib.service;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class RestClientBase {

    private static RestClientBase instance;

    private final RestTemplate restTemplate;
    private final String baseUrl;

    private RestClientBase(String baseUrl) {
        this.restTemplate = new RestTemplate();
        this.baseUrl = baseUrl;
    }

    public static synchronized RestClientBase getInstance(String baseUrl) {
        if (instance == null) {
            instance = new RestClientBase(baseUrl);
        }
        return instance;
    }

    public <T> ResponseEntity<T> get(String url, Class<T> responseType) {
        return restTemplate.exchange(url, HttpMethod.GET, null, responseType);
    }

    public <T, U> ResponseEntity<T> post(String url, U requestBody, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<U> request = new HttpEntity<>(requestBody, headers);
        return restTemplate.exchange(url, HttpMethod.POST, request, responseType);
    }

    public <T, U> ResponseEntity<T> put(String url, U requestBody, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<U> request = new HttpEntity<>(requestBody, headers);
        return restTemplate.exchange(url, HttpMethod.PUT, request, responseType);
    }
    public static String buildUrl(ServicePort servicePort, String endpoint) {
        return instance.baseUrl + servicePort.getPort() + endpoint;
    }
}
