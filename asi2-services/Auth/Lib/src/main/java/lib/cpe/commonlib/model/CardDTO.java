	package lib.cpe.commonlib.model;

	public class CardDTO {
		private Integer id;
		private Float energy;
		private Float hp;
		private Float defence;
		private Float attack;
		private Float price;
		private Integer userId;
		private String name;
		private String description;
		private String imgUrl;
		private String smallImgUrl;
		private String family;
		private String affinity;

		public CardDTO() {
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Float getEnergy() {
			return energy;
		}

		public void setEnergy(Float energy) {
			this.energy = energy;
		}

		public Float getHp() {
			return hp;
		}

		public void setHp(Float hp) {
			this.hp = hp;
		}

		public Float getDefence() {
			return defence;
		}

		public void setDefence(Float defence) {
			this.defence = defence;
		}

		public Float getAttack() {
			return attack;
		}

		public void setAttack(Float attack) {
			this.attack = attack;
		}

		public Float getPrice() {
			return price;
		}

		public void setPrice(Float price) {
			this.price = price;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getImgUrl() {
			return imgUrl;
		}

		public void setImgUrl(String imgUrl) {
			this.imgUrl = imgUrl;
		}

		public String getSmallImgUrl() {
			return smallImgUrl;
		}

		public void setSmallImgUrl(String smallImgUrl) {
			this.smallImgUrl = smallImgUrl;
		}

		public String getFamily() {
			return family;
		}

		public void setFamily(String family) {
			this.family = family;
		}

		public String getAffinity() {
			return affinity;
		}

		public void setAffinity(String affinity) {
			this.affinity = affinity;
		}
	}
