package lib.cpe.commonlib.service;

import lib.cpe.commonlib.model.AuthDTO;
import lib.cpe.commonlib.model.UserDTO;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class UserApi {
    private final String BASE_URL = "http://user-service:8082/users/";
    private final RestTemplate restTemplate;

    public UserApi() {
        this.restTemplate = new RestTemplate();
    }

    public List<UserDTO> getAllUsers() {
        ResponseEntity<UserDTO[]> response = restTemplate.getForEntity(BASE_URL, UserDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public UserDTO getUserById(Integer id) {
        ResponseEntity<UserDTO> response = restTemplate.getForEntity(BASE_URL + id, UserDTO.class);
        return response.getBody();
    }

    public void addUser(UserDTO user) {
        restTemplate.postForEntity(BASE_URL, user, String.class);
    }

    public void updateUser(UserDTO user) {
        restTemplate.put(BASE_URL + user.getId(), user);
    }

    public void deleteUser(Integer id) {
        restTemplate.delete(BASE_URL + id);
    }

    public UserDTO verifyLogin(AuthDTO authDTO) {
        try {
            ResponseEntity<UserDTO> response = restTemplate.postForEntity(
                    BASE_URL + "verify",
                    authDTO,
                    UserDTO.class
            );
            if (response.getStatusCode() == HttpStatus.OK){
                return response.getBody();
            }
        }catch (Exception ignored){}

        return null;
    }

    public UserDTO doesUserExist(Integer id) {
        ResponseEntity<UserDTO> response = restTemplate.getForEntity(BASE_URL + "exist/" + id, UserDTO.class);
        return response.getBody();
    }

    public UserDTO verifyMoney(Integer id, Integer price) {
        ResponseEntity<UserDTO> response = restTemplate.getForEntity(BASE_URL + "haveenough/" + id + "/" + price, UserDTO.class);
        return response.getBody();
    }
}
