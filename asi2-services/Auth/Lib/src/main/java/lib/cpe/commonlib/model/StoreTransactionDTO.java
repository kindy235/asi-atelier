package lib.cpe.commonlib.model;

public class StoreTransactionDTO {

    private Integer id;
    private Integer userId;
    private Integer cardId;
    private Float price;
    private StoreAction action;
    private java.sql.Timestamp timeSt;

    public StoreTransactionDTO() {
        // Default constructor
    }

    public StoreTransactionDTO(Integer id, Integer userId, Integer cardId, Float price, StoreAction action, java.sql.Timestamp timeSt) {
        this.id = id;
        this.userId = userId;
        this.cardId = cardId;
        this.price = price;
        this.action = action;
        this.timeSt = timeSt;
    }

    // Getters and Setters for each field

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public StoreAction getAction() {
        return action;
    }

    public void setAction(StoreAction action) {
        this.action = action;
    }

    public java.sql.Timestamp getTimeSt() {
        return timeSt;
    }

    public void setTimeSt(java.sql.Timestamp timeSt) {
        this.timeSt = timeSt;
    }
}
