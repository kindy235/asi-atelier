package com.cpe.springboot.auth.controller;


import lib.cpe.commonlib.model.AuthDTO;
import lib.cpe.commonlib.model.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;


@RestController("auth")
@CrossOrigin(origins = "http://localhost:8000", allowCredentials = "true") // Autorise les requêtes depuis ce domaine
@RequestMapping("/auth")
public class AuthRestController {
    private final AuthService authService;

    public AuthRestController() {
        this.authService = new AuthService();
    }

    @RequestMapping(method=RequestMethod.POST,value="/login")
    private Integer getAuth(@RequestBody AuthDTO authDto, HttpServletResponse response) {
        UserDTO user = authService.getUserByLoginPwd(authDto);
        if(user != null) {
            Cookie cookie = new Cookie("userId", String.valueOf(user.getId()));
            cookie.setMaxAge(3600); // Set the cookie expiration time in seconds

            // Add the cookie to the response
            response.addCookie(cookie);
            return user.getId();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Authentification Failed",null);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/check-auth")
    private ResponseEntity<UserDTO> checkAuth(@CookieValue(value = "userId", required = false) String userId, HttpServletResponse response) {
        if (userId != null) {
            // Convert the user ID from the cookie to an Integer
            try {
                Integer intUserId = Integer.parseInt(userId);
                UserDTO user = authService.getUserById(intUserId);
                if (user != null) {
                    // Return the user if found
                    return ResponseEntity.ok(user);
                }
            } catch (NumberFormatException e) {
                // Handle the case where the userID from the cookie is not a valid Integer
            }
        }

        // Return an error response if the ID is not found or the user is not found
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }

    @RequestMapping(method = RequestMethod.POST, value = "/logout")
    private ResponseEntity<Void> logout(HttpServletResponse response) {
        // Create a new cookie with the same name and set its max age to 0 (to delete it)
        Cookie cookie = new Cookie("userId", null);
        cookie.setMaxAge(0);

        // Add the cookie to the response to remove it from the client
        response.addCookie(cookie);

        // Return a successful response
        return ResponseEntity.ok().build();
    }
}
