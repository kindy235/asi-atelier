package com.cpe.springboot.auth.controller;

import lib.cpe.commonlib.model.AuthDTO;
import lib.cpe.commonlib.model.UserDTO;
import lib.cpe.commonlib.service.UserApi;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthService {

    private final UserApi userApi;

    public AuthService() {
        this.userApi = new UserApi();
    }

    public UserDTO getUserByLoginPwd(AuthDTO authDTO){
        return userApi.verifyLogin(authDTO);
    }

    public UserDTO getUserById(Integer id){
        return userApi.getUserById(id);
    }
}
