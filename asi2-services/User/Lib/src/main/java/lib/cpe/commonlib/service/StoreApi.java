package lib.cpe.commonlib.service;

import lib.cpe.commonlib.model.StoreOrderDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class StoreApi {
    private final String BASE_URL = "http://store-service:8085/store/"; // Replace YOUR_PORT with the appropriate port number
    private final RestTemplate restTemplate;

    public StoreApi() {
        this.restTemplate = new RestTemplate();
    }
    public boolean buyCard(StoreOrderDTO order) {
        ResponseEntity<Boolean> response = restTemplate.postForEntity(BASE_URL + "buy", order, Boolean.class);
        return response.getBody();
    }

    public boolean sellCard(StoreOrderDTO order) {
        ResponseEntity<Boolean> response = restTemplate.postForEntity(BASE_URL + "sell", order, Boolean.class);
        return response.getBody();
    }

    public List<StoreOrderDTO> getAllTransactions() {
        ResponseEntity<StoreOrderDTO[]> response = restTemplate.getForEntity(BASE_URL + "transaction", StoreOrderDTO[].class);
        return Arrays.asList(response.getBody());
    }
}