package com.cpe.springboot.esb;


import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgreceiver.BusListener;
import com.cpe.springboot.esb.notification.NotificationService;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.service.UserService;
import com.cpe.springboot.user.utils.DTOMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.cpe.commonlib.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

@Component
public class UserServiceBusListenerImpl implements BusListener {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserService userService;

    @Autowired
    private NotificationService notificationService;

    private void doReceive(String busName, TextMessage message) {
        try {

            String clazz = message.getStringProperty("ObjectType");
            Notification<UserModel> o = (Notification<UserModel>) objectMapper.readValue(message.getText(), Class.forName(clazz));

            UserModel user = objectMapper.convertValue(o.getPayload(), UserModel.class);
            String actionName = o.getAction();

            System.out.println("[BUSLISTENER] [CHANNEL "+busName+"] RECEIVED String MSG=["+message.getText()+"]");


            if (actionName.equals("create")) {
                UserModel neUser = userService.addUser(user);
                notificationService.sendNotification("user/notify", new Notification<>(actionName, "success" ,DTOMapper.mapUserModelToDTO(neUser)));
            }
            if (actionName.equals("update")) {
                UserModel updatedUser = userService.updateUser(user);
                notificationService.sendNotification("user/notify", new Notification<>(actionName, "success", DTOMapper.mapUserModelToDTO(updatedUser)));
            }
            if (actionName.equals("delete")) {
                userService.deleteUser(user.getId().toString());
                notificationService.sendNotification("user/notify", new Notification<>(actionName, "success", null));
            }

        } catch (Exception e) {
            notificationService.sendNotification("user/notify", new Notification<>("error", "failed", null));
        }
    }

    @JmsListener(destination = "USER_SERVICE_BUS", containerFactory = "queueConnectionFactory")
    public void recvMsg(TextMessage message) {
        doReceive("USER_SERVICE_BUS", message);
    }

}
