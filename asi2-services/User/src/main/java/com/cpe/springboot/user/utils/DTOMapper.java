package com.cpe.springboot.user.utils;

import com.cpe.springboot.user.model.UserModel;
import lib.cpe.commonlib.model.CardDTO;
import lib.cpe.commonlib.model.StoreTransactionDTO;
import lib.cpe.commonlib.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class DTOMapper {
    public static UserDTO mapUserModelToDTO(UserModel user) {
        if (user == null) {
            return null;
        }

        return new UserDTO(
            user.getId(),
            user.getLogin(),
            user.getPwd(),
            user.getAccount(),
            user.getLastName(),
            user.getSurName(),
            user.getEmail(),
            user.getCardList());
    }
    public static UserModel mapUserDTOToUserModel(UserDTO user) {
        if (user == null) {
            return null;
        }
        return new UserModel(
                user.getId(),
                user.getLogin(),
                user.getPwd(),
                user.getAccount(),
                user.getLastName(),
                user.getSurName(),
                user.getEmail(),
                user.getCardList());
    }
    public static List<UserDTO> mapUserModelToDTOs(List<UserModel> cardModel) {
        List<UserDTO> userDTOS = new ArrayList<>();
        for(UserModel storeTransaction : cardModel)
        {
            userDTOS.add(mapUserModelToDTO(storeTransaction));
        }

        return userDTOS;
    }

}
