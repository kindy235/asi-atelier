package com.cpe.springboot.esb.msgemitter;

import com.cpe.springboot.esb.model.Notification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import javax.jms.TextMessage;

@Service
public class BusService<T> {

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    ObjectMapper objectMapper;

    public void sendMsg(T data, String busName) {
        System.out.println("[BUSSERVICE] SEND String MSG=["+data+"] to Bus=["+busName+"]");
        jmsTemplate.send(busName, s -> {
            try {

                TextMessage msg = s.createTextMessage(objectMapper.writeValueAsString(data));
                msg.setStringProperty("Content-Type", "application/json");
                msg.setStringProperty("ObjectType", data.getClass().getCanonicalName());

                return msg;
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
