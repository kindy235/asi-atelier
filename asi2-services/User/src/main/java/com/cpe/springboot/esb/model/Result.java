package com.cpe.springboot.esb.model;

public enum Result {
    FAILED,
    PROGRESS,
    SUCCESS
}
