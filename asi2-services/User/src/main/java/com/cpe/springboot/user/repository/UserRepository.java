package com.cpe.springboot.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.user.model.UserModel;
import org.springframework.stereotype.Repository;

public interface UserRepository extends CrudRepository<UserModel, Integer> {
	Optional<UserModel> findByLoginAndPwd(String login, String pwd);

}
