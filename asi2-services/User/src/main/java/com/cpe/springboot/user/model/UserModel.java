package com.cpe.springboot.user.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lib.cpe.commonlib.model.CardDTO;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(unique = true) // Définit que le login doit être unique
	@NotEmpty(message = "Le login ne peut pas être vide")
	private String login;

	@NotEmpty(message = "Le pwd ne peut pas être vide")
	private String pwd;
	private float account;
	private String lastName;
	private String surName;
	private String email;

	@Transient
	private Set<CardDTO> cardList = new HashSet<>();

	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(Integer id, String login, String pwd, float account, String lastName, String surName, String email, Set<CardDTO> cardList) {
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.account = account;
		this.lastName = lastName;
		this.surName = surName;
		this.email = email;
		this.cardList = cardList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Set<CardDTO> getCardList() {
		return cardList;
	}

	public void setCardList(Set<CardDTO> cardList) {
		this.cardList = cardList;
	}

	public void addAllCardList(Collection<CardDTO> cardList) {
		this.cardList.addAll(cardList);
	}


	public void addCard(CardDTO card) {
		this.cardList.add(card);
	}

	private boolean checkIfCard(CardDTO c){
		for(CardDTO u_c: this.cardList){
			if(Objects.equals(u_c.getId(), c.getId())){
				return true;
			}
		}
		return false;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
