package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.cpe.springboot.user.service.UserService;
import com.cpe.springboot.user.utils.DTOMapper;
import lib.cpe.commonlib.model.UserDTO;
import lib.cpe.commonlib.model.AuthDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.springboot.user.model.UserModel;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserRestController {

	private final UserService userService;
	
	public UserRestController(UserService userService) {
		this.userService=userService;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	private List<UserDTO> getAllUsers() {
		return DTOMapper.mapUserModelToDTOs(userService.getAllUsers());
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/{id}")
	private UserDTO getUser(@PathVariable Integer id) {

		UserModel user = userService.getUser(id);
		if(user != null) {
			return DTOMapper.mapUserModelToDTO(user);
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User id:"+id+", not found",null);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<String> addUser(@RequestBody UserDTO user) {
		userService.actionAsync(DTOMapper.mapUserDTOToUserModel(user), "create");
		return ResponseEntity.ok("User creation in progress");
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/{id}")
	public ResponseEntity<String> updateUser(@RequestBody UserDTO user,@PathVariable String id) {
		user.setId(Integer.valueOf(id));
		userService.actionAsync(DTOMapper.mapUserDTOToUserModel(user), "update");
		return ResponseEntity.ok("PUT request has been submitted asynchronously.");
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable Integer id) {
		UserModel user = userService.getUser(id);
		if (user != null){
			userService.actionAsync(user,"delete");
			return ResponseEntity.ok("DELETE request has been submitted asynchronously.");
		}
		return ResponseEntity.ok("DELETE request error");
	}

	@RequestMapping(method=RequestMethod.POST,value="/verify")
	public ResponseEntity<UserDTO> verify_login(@RequestBody AuthDTO authDTO){
		Optional<UserModel> optUser = userService.getUserByLoginPwd(authDTO.getUsername(), authDTO.getPassword());
		if (optUser.isPresent()) {
			UserDTO user = DTOMapper.mapUserModelToDTO(optUser.get());
			if (user.getLogin().equals(authDTO.getUsername()) && user.getPwd().equals(authDTO.getPassword()))
				return new ResponseEntity<>(user, HttpStatus.OK);
		}
		return new  ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/exist/{id}")
	private ResponseEntity<UserDTO> doesUserExist(@PathVariable Integer id) {
		UserModel ruser = userService.getUser(id);
		if (ruser!= null){
			// L'utilisateur existe
			return new ResponseEntity<>(DTOMapper.mapUserModelToDTO(ruser), HttpStatus.OK);
		}
		return new  ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/haveenough/{id}/{price}")
	private ResponseEntity<UserDTO> VerifMoney(@PathVariable Integer id, @PathVariable Integer price) {
		UserModel ruser = userService.getUser(id);

		if(ruser!= null){
			UserDTO user = DTOMapper.mapUserModelToDTO(ruser);

			if (user.getAccount() >= price) {
				// L'utilisateur a suffisamment d'argent
				return new ResponseEntity<>(user, HttpStatus.OK);
			}
		}
		return new  ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
}
