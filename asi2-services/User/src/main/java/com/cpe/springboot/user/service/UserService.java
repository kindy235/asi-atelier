package com.cpe.springboot.user.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgemitter.BusService;
import com.cpe.springboot.esb.notification.NotificationService;
import com.cpe.springboot.user.repository.UserRepository;
import lib.cpe.commonlib.model.CardDTO;
import lib.cpe.commonlib.service.CardApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	private final UserRepository userRepository;
	private final CardApi cardApi;

	@Autowired
	private BusService<Notification> busService;

	@Autowired
	NotificationService notificationService;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
		this.cardApi = new CardApi();
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		try {
			for (UserModel user : userList) {

				user.setCardList(new HashSet<>(cardApi.getCardByUserId(user.getId())));
			}
		}catch (Exception ignored){}

		return userList;
	}

	public UserModel getUser(Integer id) {
		Optional<UserModel> optUser = userRepository.findById(id);
		if(optUser.isPresent()){
			UserModel user = optUser.get();
			try {
				user.setCardList(new HashSet<>(cardApi.getCardByUserId(user.getId())));
			}catch (Exception ignored){}
			return user;
		}
		return null;
	}

	public UserModel addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		UserModel nUser = userRepository.save(user);
		List<CardDTO> cardList = cardApi.getRandCards(nUser.getId());
		nUser.setCardList(new HashSet<>(cardList));

		return nUser;
	}

	public UserModel updateUser(UserModel user) {
		userRepository.save(user);
		return user;
	}


	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUserByLoginPwd(String login, String pwd) {
		return userRepository.findByLoginAndPwd(login, pwd);
	}

	public void actionAsync(UserModel user, String actionName){
		CompletableFuture.runAsync(() -> {
			notificationService.sendNotification("user/notify", new Notification<>(actionName, "progress", null));
			busService.sendMsg(new Notification<>(actionName, "progress", user), "USER_SERVICE_BUS");
		});
	}
}
