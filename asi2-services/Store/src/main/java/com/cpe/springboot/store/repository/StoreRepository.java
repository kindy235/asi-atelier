package com.cpe.springboot.store.repository;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.store.model.StoreTransaction;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends CrudRepository<StoreTransaction, Integer> {
	

}
