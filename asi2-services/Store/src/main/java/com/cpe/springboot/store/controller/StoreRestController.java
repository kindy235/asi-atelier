package com.cpe.springboot.store.controller;

import com.cpe.springboot.store.model.StoreOrder;

import com.cpe.springboot.store.service.StoreService;
import com.cpe.springboot.store.utils.DTOMapper;
import lib.cpe.commonlib.model.StoreOrderDTO;
import lib.cpe.commonlib.model.StoreTransactionDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@CrossOrigin
@RestController
@RequestMapping(value="/store")
public class StoreRestController {

	private final StoreService storeService;

	public StoreRestController(StoreService storeService) {
		this.storeService = storeService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/buy")
	private ResponseEntity<String> getAllCards(@RequestBody StoreOrderDTO order) {
		storeService.actionAsync(DTOMapper.mapDTOToStoreOrder(order), "buy");
		return ResponseEntity.ok("Buy operation is in progress");
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sell")
	private ResponseEntity<String> getCard(@RequestBody StoreOrderDTO order) {
		storeService.actionAsync(DTOMapper.mapDTOToStoreOrder(order), "sell");
		return ResponseEntity.ok("Sell operation is in progress");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/transaction")
	private List<StoreTransactionDTO> getCard() {
		return DTOMapper.mapStoreTransactionToDTOs(storeService.getAllTransactions());
	}

}
