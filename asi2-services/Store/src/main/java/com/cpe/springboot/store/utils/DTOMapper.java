package com.cpe.springboot.store.utils;

import com.cpe.springboot.store.model.StoreOrder;
import com.cpe.springboot.store.model.StoreTransaction;
import lib.cpe.commonlib.model.CardDTO;
import lib.cpe.commonlib.model.StoreOrderDTO;
import lib.cpe.commonlib.model.StoreTransactionDTO;
import org.apache.catalina.Store;

import java.util.ArrayList;
import java.util.List;

public class DTOMapper {

    public static StoreTransactionDTO mapStoreTransactionToDTO(StoreTransaction transaction) {
        if (transaction == null) {
            return null;
        }

        return new StoreTransactionDTO(
                transaction.getId(),
                transaction.getUserId(),
                transaction.getCardId(),
                transaction.getPrice(),
                transaction.getAction(),
                transaction.getTimeSt()
        );
    }

    public static List<StoreTransactionDTO> mapStoreTransactionToDTOs(List<StoreTransaction> cardModel) {
        List<StoreTransactionDTO> transactionDTOS = new ArrayList<>();
        for(StoreTransaction storeTransaction : cardModel)
        {
            transactionDTOS.add(mapStoreTransactionToDTO(storeTransaction));
        }

        return transactionDTOS;
    }

    public static StoreOrderDTO mapStoreOrderToDTO(StoreOrder order) {
        if (order == null) {
            return null;
        }

        return new StoreOrderDTO(
                order.getUser_id(),
                order.getCard_id()
        );
    }

    public static StoreTransaction mapDTOToStoreTransaction(StoreTransactionDTO transactionDTO) {
        if (transactionDTO == null) {
            return null;
        }

        StoreTransaction transaction = new StoreTransaction();
        transaction.setId(transactionDTO.getId());
        transaction.setUserId(transactionDTO.getUserId());
        transaction.setCardId(transactionDTO.getCardId());
        transaction.setPrice(transactionDTO.getPrice());
        transaction.setAction(transactionDTO.getAction());
        transaction.setTimeSt(transactionDTO.getTimeSt());

        return transaction;
    }

    public static StoreOrder mapDTOToStoreOrder(StoreOrderDTO orderDTO) {
        if (orderDTO == null) {
            return null;
        }

        StoreOrder order = new StoreOrder();
        order.setUser_id(orderDTO.getUserId());
        order.setCard_id(orderDTO.getCardId());

        return order;
    }


}
