package com.cpe.springboot.esb;


import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgreceiver.BusListener;
import com.cpe.springboot.esb.notification.NotificationService;
import com.cpe.springboot.store.model.StoreOrder;
import com.cpe.springboot.store.model.StoreTransaction;
import com.cpe.springboot.store.service.StoreService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;

@Component
public class StoreServiceBusListenerImpl implements BusListener {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    StoreService storeService;

    @Autowired
    private NotificationService notificationService;

    private void doReceive(String busName, TextMessage message) {
        try {
            String clazz = message.getStringProperty("ObjectType");
            Notification<StoreOrder> o = (Notification<StoreOrder>) objectMapper.readValue(message.getText(), Class.forName(clazz));

            StoreOrder order = objectMapper.convertValue(o.getPayload(), StoreOrder.class);
            String actionName = o.getAction();
            if (actionName.equals("sell")) {
                boolean isOk = storeService.sellCard(order.getUser_id(), order.getCard_id());
                notificationService.sendNotification("store/notify", new Notification<>(actionName, "success", isOk));
            }
            if (actionName.equals("buy")) {
                boolean isOk = storeService.buyCard(order.getUser_id(), order.getCard_id());
                notificationService.sendNotification("store/notify", new Notification<>(actionName, "success", isOk));
            }

            System.out.println("[BUSLISTENER] [CHANNEL "+busName+"] RECEIVED String MSG=["+message.getText()+"]");

        } catch (Exception e) {
            notificationService.sendNotification("store/notify", new Notification<>("error", "failed", null));
            System.out.println("[BUSLISTENER] [CHANNEL "+busName+"] ERROR String MSG=["+e.getMessage()+"]");
        }
    }

    @JmsListener(destination = "STORE_SERVICE_BUS", containerFactory = "queueConnectionFactory")
    public void recvMsg(TextMessage message) {
        doReceive("STORE_SERVICE_BUS", message);
    }

}
