package com.cpe.springboot.store.service;


import com.cpe.springboot.esb.model.Notification;
import com.cpe.springboot.esb.msgemitter.BusService;
import com.cpe.springboot.esb.notification.NotificationService;
import com.cpe.springboot.store.model.StoreOrder;
import com.cpe.springboot.store.model.StoreTransaction;
import com.cpe.springboot.store.repository.StoreRepository;
import lib.cpe.commonlib.model.CardDTO;
import lib.cpe.commonlib.model.StoreAction;
import lib.cpe.commonlib.model.UserDTO;
import lib.cpe.commonlib.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class StoreService {

	private final StoreRepository storeRepository;
	private final RestClientBase restClientBase;
	private final String baseUrl;

	private final CardApi cardApi;
	private final UserApi userApi;
	@Autowired
	private BusService<Notification> busService;
	@Autowired
	NotificationService notificationService;

	public StoreService(StoreRepository storeRepositor) {
		this.storeRepository = storeRepositor;
		this.cardApi = new CardApi();
		this.userApi = new UserApi();
		this.baseUrl = "http://localhost:8085";
		this.restClientBase = RestClientBase.getInstance(baseUrl);
	}

	public boolean buyCard(Integer userId, Integer cardId) {
		UserDTO user = userApi.getUserById(userId);
		CardDTO card = cardApi.getCardRefById(cardId);

		if (user != null && card != null) {
			Float price = card.getPrice();
			Float userAccount = user.getAccount();

			if (userAccount >= price) {
				// L'utilisateur a assez d'argent pour acheter la carte
				user.setAccount(userAccount - price); // Déduire le prix de la carte du compte de l'utilisateur
				card.setUserId(userId); // Mettre à jour le propriétaire de la carte avec l'ID de l'utilisateur
				card.setId(null);

				StoreTransaction storeTransaction = new StoreTransaction(userId,cardId, price, StoreAction.BUY);
				// Mettre à jour l'utilisateur et la carte dans la base de données
				userApi.updateUser(user);
				cardApi.addCard(card);
				storeRepository.save(storeTransaction);
				return true;
			}
		}
		return false; // Retourner null si l'utilisateur ou la carte n'existe pas ou si l'utilisateur n'a pas assez d'argent
	}

	public boolean sellCard(Integer userId, Integer cardId) {
		UserDTO user = userApi.getUserById(userId);
		CardDTO card = cardApi.getCardById(cardId);

		if (user != null && card != null)
			if(Objects.equals(card.getUserId(), userId)){
			Float price = card.getPrice();
			Float userAccount = user.getAccount();
			// L'utilisateur a assez d'argent pour acheter la carte
			user.setAccount(userAccount + price); // Déduire le prix de la carte du compte de l'utilisateur
			card.setUserId(null); // Mettre à jour le propriétaire de la carte avec l'ID de l'utilisateur

			StoreTransaction storeTransaction = new StoreTransaction(userId,cardId, price, StoreAction.SELL);
			// Mettre à jour l'utilisateur et la carte dans la base de données
			userApi.updateUser(user);
			cardApi.updateCard(card);

			storeRepository.save(storeTransaction);
			return true;
		}
		return false; // Retourner null si l'utilisateur ou la carte n'existe pas ou si l'utilisateur n'a pas assez d'argent
	}

	public List<StoreTransaction> getAllTransactions() {
		List<StoreTransaction> transactionsList = new ArrayList<>();
		storeRepository.findAll().forEach(transactionsList::add);
		return transactionsList;
	}

	public void actionAsync(StoreOrder order, String actionName) {
		CompletableFuture.runAsync(() -> {
			notificationService.sendNotification("store/notify", new Notification<>(actionName, "progress", null));
			busService.sendMsg(new Notification<>(actionName, "progress", order), "STORE_SERVICE_BUS");
		});
	}
}
