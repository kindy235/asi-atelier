const express = require('express');
const cors = require('cors'); // Import the cors middleware
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');

app.use(bodyParser.json());
app.use(cors()); // Add cors middleware to your Express app

// Stockage des parties en mémoire
const games = {};

// Endpoint pour créer une nouvelle partie
app.post('/createGame', (req, res) => {
  const { playerId, playerCards } = req.body;
  const gameCode = generateGameCode();
  //console.log(req.body)
  games[gameCode] = {
    player1: {
      id: playerId,
      cards: playerCards,
    },
    player2: {
      id: -1,
      cards: [],
    },
    activePlayerId: playerId,
    winnerPlayerId: -1
  };
  
  return res.json({gameCode});
});

// Endpoint pour rejoindre une partie existante
app.post('/joinRandomGame', (req, res) => {
  const { playerId, playerCards } = req.body;
  //console.log(req.body)
  //console.log("joinRandomGame")
  //console.log(games)
  let gameCode = ""
  if (Object.keys(games).length > 0){
    gameCode = Object.keys(games)[0];
    //console.log(gameCode)
  }else{
    res.status(404).json({ error: 'No game found' });
  }


  if (games[gameCode]) {
    games[gameCode].player2.id = playerId;
    games[gameCode].player2.cards = playerCards;
    return res.json({gameCode});
  } else {
    return res.status(404).json({ error: 'Game not found' });
  }
});

// Endpoint pour rejoindre une partie existante
app.post('/joinGame', (req, res) => {
  const { gameCode, playerId, playerCards } = req.body;
  //console.log(req.body)
  if (games[gameCode]) {
    games[gameCode].player2.id = playerId;
    games[gameCode].player2.cards = playerCards;
    res.json({ message: 'Joined the game successfully' });
  } else {
    res.status(404).json({ error: 'Game not found' });
  }
});

// Endpoint pour récupérer les informations d'une partie
app.get('/getGame', (req, res) => {
  const { gameCode } = req.query;
  
  if (games[gameCode]) {
    res.json(games[gameCode]);
  } else {
    res.status(404).json({ error: 'Game not found' });
  }
});

// Endpoint pour effectuer une attaque d'un joueur
app.post('/playerAttack', (req, res) => {
  const { gameCode, CarteSource, CarteDest } = req.body;
  
  if (games[gameCode]) {
    const game = games[gameCode];
    const activePlayer = game.activePlayerId === game.player1.id ? game.player1 : game.player2;
    const opponent = game.activePlayerId === game.player1.id ? game.player2 : game.player1;

    console.log("serv attack")
    //console.log(activePlayer)
    //console.log(opponent)
    //console.log("Joueur actif",activePlayer.id)
    //console.log("Joueur opposant",opponent.id)
    //console.log("Carte qui attack", CarteSource)
    //console.log("Carte qui defend",CarteDest)
    // console.log(game.player1.cards)

    //console.log("\n\n\n Joueur actif card ",activePlayer)

    let attackingCard = ""
    
    // La carte du joueur actif inflige des dégâts à la carte du joueur inactif
    activePlayer.cards.forEach(card => {
      if (parseInt(card.id) === parseInt(CarteSource)) {
        //console.log(card)
        attackingCard = parseFloat(card.attack);
        //console.log("DEGATT = ",attackingCard)
      }
    });

    console.log("\n\n------------------\n\n")

    // La carte du joueur actif inflige des dégâts à la carte du joueur inactif
    opponent.cards.forEach(card => {
      if (parseInt(card.id) == parseInt(CarteDest)) {
        //console.log(card)
        //console.log("Carte parser = ",card.id)
        //console.log("PV AVANT",parseFloat(card.hp))
        card.hp = parseFloat(card.hp) - attackingCard;
        //console.log("PV APRES",card.hp)
        
        // Vérifier si une carte a été détruite
        if (parseFloat(card.hp) <= 0) {
          //console.log("SUPPRESION DE LA CARTE ",card.id)
          // Supprimer la carte
          card.hp = 0
          card.energy = 0
          card.defence = 0
          card.attack = 0
        }
      }
    });

    let compteur = 0
    console.log("Compteur avant = ",compteur)
    // Verifie qu'il y a toujours des cartes en vie
    opponent.cards.forEach(card => {
      if (parseFloat(card.hp) == 0) {
        compteur = compteur + 1
        //console.log("Compteur en cours = ",compteur)
        if (compteur === 3){
          //console.log("FIN DE LA GAME Compteur = ",compteur)
          //console.log("Winner avant = ",game.winnerPlayerId)
          game.winnerPlayerId = activePlayer.id
          //console.log("Winner apres = ",game.winnerPlayerId)
        }
      }
    });
    //console.log("Nombre de carte morte = ",compteur)

    //la game continue on change de joueur actif
    if(game.activePlayerId == game.player1.id){
      game.activePlayerId = game.player2.id
    }else{
      game.activePlayerId = game.player1.id
    }
 
    return res.json({ message: 'Attack successful' });
    } else {
      res.status(404).json({ error: 'Game not found' });
    }
});

// Fonction pour générer un code de partie aléatoire
function generateGameCode() {
  return Math.random().toString(36).substring(7);
}

// Démarrer le serveur
app.listen(3200, () => {
  console.log('Server is running on port 3200');
});