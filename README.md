# ASI ATELIER 1

## GROUPE 2
Emre Acet\
Aboubacar Bah\
Luca Polgar\
Yamine Kebaili

## activités réalisées (par personne)
Aboubacar : Back (User Service, ESB) + Front (Store Buy et Sell)

Emre : Back (Authentification service) + Front (Authentification puis Store Buy et Sell)

Luca : Serveur de Notification, ActiveMQ, Commencement du Tchat

Yamine :  Back (Store service + Card service + mise en place des libs + BDD)  

GITLAB repo : https://gitlab.com/kindy235/asi-atelier.git

## Liste des éléments réalisés :
- Authentification d'un utilisateur avec les cookies
- Page d'accueil avec 3 boutons (Buy, Sell, Play)
- Mis en plage des 3 pages (Buy, Sell, Play)
- Front réalisées avec découpage REACTJS en composant et utilisation de Redux
- Interraction Back-Front
- Communication Serveur de notification avec Client websocket front
- Communication micro services avec Serveur de notification
- Achat et vente de cartes
- Docker compose pour tous les services (front+back+notification)
- GitLab Pipeline 

## Liste des éléments non-réalisés
--

